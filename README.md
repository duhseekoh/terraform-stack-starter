aws-terraform-stack-starter
===========================

**WIP**

Opinionated AWS+Terraform starter stack. Stands up:

- A VPC
- _N_ public subnets in _N_ availability zones
  - Auto-assigned public IPs
  - Egress through an Internet Gateway
- _N_ private subnets in _N_ availability zones
  - Egress through NAT gateways
- Standardized security groups for public and private resources
- A bastion host that is publicly accessible through SSH and has SSH access to private resources.

Architecture
------------

![Architecture diagram](./network_diagram.png)
