resource "aws_vpc" "vpc" {
  cidr_block           = "${var.cidr_block}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    "Name"      = "${var.vpc_name}"
    "terraform" = "true"
  }
}

resource "aws_route53_zone" "zone" {
  name   = "${var.vpc_name}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    terraform = "true"
  }
}
