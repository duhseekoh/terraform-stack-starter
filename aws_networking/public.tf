resource "aws_subnet" "public" {
  count = "${length(var.public_subnets)}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnets[count.index]}"
  availability_zone       = "${var.subnet_availability_zones[count.index]}"
  map_public_ip_on_launch = true

  tags = {
    "Name"      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-public"
    "terraform" = "true"
  }
}

resource "aws_route_table" "public" {
  count = "${length(var.public_subnets)}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-public"
    "terraform" = "true"
  }
}

resource "aws_route_table_association" "public" {
  count          = "${length(var.public_subnets)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.public.*.id, count.index)}"
}

## Provides route to Internet for public subnets
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-igw"
    "terraform" = "true"
  }
}

resource "aws_route" "public_to_internet" {
  count = "${length(var.public_subnets)}"

  route_table_id         = "${element(aws_route_table.public.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${element(aws_internet_gateway.internet_gateway.*.id, count.index)}"
}
