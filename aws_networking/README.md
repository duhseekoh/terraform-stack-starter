aws_network
===========

Terraform module to configure essential AWS networking.

- VPC
- Bastion host with
  - SSH routing to public and private subnets
  - An ASG to keep the bastion alive if an AZ goes down
- _N_ public subnets with
  - IGW for egress
- _N_ private subnets, each with
  - NAT gateways for egress
