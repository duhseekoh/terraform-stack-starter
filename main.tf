provider "aws" {
  version = "~> 1.7"
  region  = "${var.aws_region}"
}

provider "template" {
  version = "~> 1.0"
}

module "networking" {
  source = "./aws_networking"

  vpc_name                  = "${var.stack_name}"
  subnet_availability_zones = ["us-east-1a", "us-east-1b"]
}

module "bastion" {
  source = "./aws_autoscaling_group"

  prefixes      = ["${var.stack_name}"]
  name          = "bastion"
  vpc_id        = "${module.networking.vpc_id}"
  key_pair_name = "${var.stack_name}"
  size_min      = 1
  size_max      = 1
  size_desired  = 1

  security_group_ids = [
    "${module.networking.security_group_id_default}",
    "${module.networking.security_group_id_default_public}",
  ]

  subnet_ids = ["${module.networking.subnet_ids_public}"]
}

resource "aws_security_group_rule" "world_to_bastion_ssh" {
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  security_group_id = "${module.bastion.security_group_id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion_to_private_ssh" {
  type                     = "ingress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  security_group_id        = "${module.networking.security_group_id_default_private}"
  source_security_group_id = "${module.bastion.security_group_id}"
}
