variable "stack_name" {
  default = "my-stack"
}

variable "aws_region" {
  default = "us-east-1"
}
