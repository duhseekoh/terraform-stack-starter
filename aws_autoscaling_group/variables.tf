variable "prefixes" {
  type    = "list"
  default = []
}

variable "name" {
  type = "string"
}

variable "vpc_id" {
  type = "string"
}

variable "ami_id" {
  type    = "string"
  default = ""
}

variable "instance_type" {
  type    = "string"
  default = "t2.micro"
}

variable "key_pair_name" {
  type = "string"
}

variable "security_group_ids" {
  type    = "list"
  default = []
}

variable "size_min" {
  type = "string"
}

variable "size_max" {
  type = "string"
}

variable "size_desired" {
  type = "string"
}

variable "subnet_ids" {
  type = "list"
}

variable "user_data" {
  type    = "string"
  default = ""
}
